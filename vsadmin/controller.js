/**
 * Created by hungmai on 15/06/2016.
 */
var app = angular.module('ProductAdmin', ['ui.router']);

app.factory('principal', ['$q', '$timeout',
    function($q, $timeout) {
        var _identity = undefined,
            _authenticated = false;

        return {

            isIdentityResolved: function() {
                return angular.isDefined(_identity);
            },
            isAuthenticated: function() {
                return _authenticated;
            },
            isInRole: function(role) {
                if (!_authenticated || !_identity.roles) return false;

                return _identity.roles.indexOf(role) != -1;
            },
            isInAnyRole: function(roles) {
                if (!_authenticated || !_identity.roles) return false;

                for (var i = 0; i < roles.length; i++) {
                    if (this.isInRole(roles[i])) return true;
                }

                return false;
            },
            authenticate: function(identity) {
                _identity = identity;
                _authenticated = identity != null;

                // for this demo, we'll store the identity in localStorage. For you, it could be a cookie, sessionStorage, whatever
                if (identity) localStorage.setItem("demo.identity", angular.toJson(identity));
                else localStorage.removeItem("demo.identity");
            },
            identity: function(force) {
                var deferred = $q.defer();

                if (force === true) _identity = undefined;

                // check and see if we have retrieved the identity data from the server. if we have, reuse it by immediately resolving
                if (angular.isDefined(_identity)) {
                    deferred.resolve(_identity);

                    return deferred.promise;
                }

                var self = this;
                $timeout(function($scope) {
                    _identity = angular.fromJson(localStorage.getItem("demo.identity"));
                    self.authenticate(_identity);
                    deferred.resolve(_identity);
                }, 1000);

                return deferred.promise;
            }
        };
    }
]);

app.factory('authorization', ['$rootScope', '$state', 'principal',
    function($rootScope, $state, principal) {
        return {
            authorize: function() {
                return principal.identity()
                    .then(function() {
                        var isAuthenticated = principal.isAuthenticated();

                        if ($rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !principal.isInAnyRole($rootScope.toState.data.roles)) {
                            if (isAuthenticated) $state.go('deny'); // user is signed in but not authorized for desired state
                            else {
                                // user is not authenticated. stow the state they wanted before you
                                // send them to the signin state, so you can return them when you're done
                                $rootScope.returnToState = $rootScope.toState;
                                $rootScope.returnToStateParams = $rootScope.toStateParams;
                                $rootScope.showLogout = false;

                                // now, send them to the signin state so they can log in
                                $state.go('login');
                            }
                        }
                    });
            }
        };
    }
]);

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/main');
    $stateProvider
        .state('authorize', {
            'abstract': true,
            resolve: {
                authorize: ['authorization', function(authorization){
                    return authorization.authorize();
                }]
            }
        })
        .state('main', {
            parent: 'authorize',
            url: '/main',
            views: {
                'content@': {
                    templateUrl: 'productadmin_main.html',
                    controller: 'ProductAdminController'
                }
            },
            data: {
                roles: ['Admin']
            },
            resolve: {
                Products: ['$q', function ($q, $scope) {
                    var deferred = $q.defer();
                    var products = [];

                    firebase.database().ref('products').on('value', function (snapshot) {
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[0].apple);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[1].samsung);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[2].htc);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[3].sony);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[4].oppo);
                        Array.prototype.push.apply(products, snapshot.val()[1].tablet[0].apple);
                        Array.prototype.push.apply(products, snapshot.val()[1].tablet[1].samsung);
                        Array.prototype.push.apply(products, snapshot.val()[1].tablet[2].asus);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[0].dell);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[1].asus);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[2].hp);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[3].lenovo);
                        deferred.resolve(products);
                    });

                    return deferred.promise;
                }]
            }
        })
        .state('newproduct', {
            //parent: 'authorize',
            url: '/newproduct',
            views: {
                'content@': {
                    templateUrl: 'newproduct.html',
                    controller: 'NewProductController'
                }
            },
            data: {
                roles: ['Admin']
            }
        })
        .state('modifyproduct', {
            //parent: 'authorize',
            url: '/modifyproduct/:productID',
            views: {
                'content@': {
                    templateUrl: 'newproduct.html',
                    controller: 'ModifyProductController'
                }
            },
            data: {
                roles: ['Admin']
            }
        })
        .state('login', {
            parent: 'authorize',
            url: '/login',
            views: {
                'content@': {
                    templateUrl: 'login.html',
                    controller: 'LoginController'
                }
            },
            data: {
                roles: []
            }
        })
        .state('changepass', {
            parent: 'authorize',
            url: '/changepass',
            views: {
                'content@': {
                    templateUrl: 'changepass.html',
                    controller: 'ChangePassController'
                }
            },
            data: {
                roles: ['Admin']
            }
        })
        .state('deny', {
            parent: 'authorize',
            url: '/deny',
            views: {
                'content@': {
                    templateUrl: 'deny_page.html'
                }
            },
            data: {
                roles: ['Admin']
            }
        });
}).run(['$rootScope', '$state', '$stateParams', 'authorization', 'principal',
    function($rootScope, $state, $stateParams, authorization, principal) {
        $rootScope.$on('$stateChangeStart', function(event, toState, toStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;

            if (principal.isIdentityResolved()){
                authorization.authorize();
            }
        });
    }
]);

app.controller('MainController', function($scope, $state, principal){
    $scope.isShow = false;
    $scope.email = '';

    $scope.ShowHeader = function(){
        $scope.isShow = true;
    }

    $scope.HideHeader = function(){
        $scope.isShow = false;
    }

    $scope.logout = function(){
        firebase.auth().signOut().then(function() {
            // Sign-out successful.
        }, function(error) {
            // An error happened.
        });
        principal.authenticate(null);
        $state.go('login');
    }

    $scope.ShowLogout = function(){
        $scope.showLogout = true;
    }

    $scope.HideLogout = function(){
        $scope.showLogout = false;
    }
});

app.controller('ProductAdminController', function($scope, $state, Products) {
    $scope.ShowHeader();

    $scope.products = Products;
    localStorage.setItem('products', JSON.stringify($scope.products));
    $scope.deleteProduct = function(index){
        var category = '';
        if ($scope.products[index].id.substring(0,2) == 'mb'){
            category = 'mobile';
        }else if ($scope.products[index].id.substring(0,2) == 'tl'){
            category = 'tablet';
        }else {
            category = 'laptop';
        }
        var brand = $scope.products[index].brand.toLowerCase();
        var indexCategory = 0;
        var indexBrand = 0;
        if (category == 'mobile'){
            indexCategory = 0;
            if (brand == 'apple'){
                indexBrand = 0;
            }else if (brand == 'samsung'){
                indexBrand = 1;
            }else if (brand == 'htc'){
                indexBrand = 2;
            }else if (brand == 'sony'){
                indexBrand = 3;
            }else if (brand == 'oppo'){
                indexBrand = 4;
            };
        }else if (category == 'tablet'){
            indexCategory = 1;
            if (brand == 'apple'){
                indexBrand = 0;
            }else if (brand == 'samsung'){
                indexBrand = 1;
            }else if (brand == 'asus'){
                indexBrand = 2;
            };
        }else if (category == 'laptop'){
            indexCategory = 2;
            if (brand == 'dell'){
                indexBrand = 0;
            }else if (brand == 'asus'){
                indexBrand = 1;
            }else if (brand == 'hp'){
                indexBrand = 2;
            }else if (brand == 'lenovo'){
                indexBrand = 3;
            };
        }

        var indexItem = parseInt($scope.products[index].id.substr($scope.products[index].id.length - 1), 10);
        var splits = $scope.products[index].id.split('-');
        indexItem = parseInt(splits[2], 10);
        indexItem -= 1;

        var string_ref = 'products/' + indexCategory + '/' + category + '/' + indexBrand + '/' + brand;

        console.log(string_ref + '/' + indexItem);
        var ref = firebase.database().ref(string_ref);
        ref.on('child_removed', function(snapshot){
            console.log('ok');
            $scope.products.splice(index, 1);
        });
        var targetProduct = ref.child(indexItem);
        console.log(indexItem);
        targetProduct.remove();
    };
    
    $scope.isShowList = true;
    
    $scope.showListProduct = function(){
        $scope.isShowList = !$scope.isShowList;
    }
});

app.controller('NewProductController', function($scope){
    $scope.newproduct = {};
    $scope.newproduct.thumnail = [];
    $scope.newproduct.images = [];
    $scope.submitProduct = function(){
        $scope.newproduct.name = document.getElementById('name').value;
        $scope.newproduct.newprice = parseInt(document.getElementById('price').value, 10);
        $scope.newproduct.availability = document.getElementById('availability').value;
        $scope.newproduct.weight = document.getElementById('weight').value + 'g';
        $scope.newproduct.brand = document.getElementById('brand').value;
        $scope.newproduct.star = parseInt(document.getElementById('star').value, 10);
        $scope.newproduct.sale = document.getElementById('sale').value + '%';
        $scope.newproduct.dimension = document.getElementById('dimension_1').value + ' x ' + document.getElementById('dimension_2').value + ' x ' + document.getElementById('dimension_3').value + ' cm';
        $scope.newproduct.colors = $scope.colors;
        $scope.newproduct.sizes = $scope.sizes;
        $scope.newproduct.overview = document.getElementById('overview').value.split('\n');
        $scope.newproduct.description = document.getElementById('description').value.split('\n');
        $scope.newproduct.oldprice = $scope.newproduct.newprice;
        $scope.newproduct.id = document.getElementById('id').value;

        $scope.uploadAllFile();
    };

    $scope.isProcess = true;

    $scope.uploadAllFile = function(){
        $scope.isProcess = false;
        document.getElementById('processing').innerHTML = 'Đang xử lý...';
        var file_thumb_1 = document.getElementById('thumb_1').files[0];
        var file_thumb_2 = document.getElementById('thumb_2').files[0];

        var file_large_1 = document.getElementById('large_1').files[0];
        var file_large_2 = document.getElementById('large_2').files[0];
        var file_large_3 = document.getElementById('large_3').files[0];
        var file_large_4 = document.getElementById('large_4').files[0];

        var file_images = [file_thumb_1, file_thumb_2, file_large_1, file_large_2, file_large_3, file_large_4];
        for (var index in file_images){
            if (file_images[index] == undefined){
                console.log('ok');
                document.getElementById('processing').innerHTML = 'File ảnh chưa được thêm vào';
                return;
            }
        }

        var storageRef = firebase.storage().ref();
        var upload_thumb_Task_1 = storageRef.child('images/' + file_thumb_1.name).put(file_thumb_1);
        upload_thumb_Task_1.on('state_changed', function(snapshot){

        }, function(error){
            console.log(error);
        }, function(){
            var downloadUrl = upload_thumb_Task_1.snapshot.downloadURL;
            $scope.newproduct.thumnail.push(downloadUrl);
            var upload_thumb_Task_2 = storageRef.child('images/' + file_thumb_2.name).put(file_thumb_2);
            upload_thumb_Task_2.on('state_changed', function(snapshot){

            }, function(error){

            }, function(){
                var downloadUrl = upload_thumb_Task_2.snapshot.downloadURL;
                $scope.newproduct.thumnail.push(downloadUrl);

                var upload_large_Task_1 = storageRef.child('images/' + file_large_1.name).put(file_large_1);
                upload_large_Task_1.on('state_changed', function(snapshot){

                }, function(error){

                }, function(){
                    var downloadUrl = upload_large_Task_1.snapshot.downloadURL;
                    $scope.newproduct.images.push(downloadUrl);

                    var upload_large_Task_2 = storageRef.child('images/' + file_large_2.name).put(file_large_2);
                    upload_large_Task_2.on('state_changed', function(snapshot){

                    }, function(error){

                    }, function(){
                        var downloadUrl = upload_large_Task_2.snapshot.downloadURL;
                        $scope.newproduct.images.push(downloadUrl);

                        var upload_large_Task_3 = storageRef.child('images/' + file_large_3.name).put(file_large_3);
                        upload_large_Task_3.on('state_changed', function(snapshot){

                        }, function(error){

                        }, function(){
                            var downloadUrl = upload_large_Task_3.snapshot.downloadURL;
                            $scope.newproduct.images.push(downloadUrl);

                            var upload_large_Task_4 = storageRef.child('images/' + file_large_4.name).put(file_large_4);
                            upload_large_Task_4.on('state_changed', function(snapshot){

                            }, function(error){

                            }, function(){
                                var downloadUrl = upload_large_Task_4.snapshot.downloadURL;
                                $scope.newproduct.images.push(downloadUrl);
                                $scope.uploadData();
                                console.log($scope.newproduct);
                            });
                        });
                    });
                });
            });

        });
    };

    $scope.reload = function(){
        location.reload();
    }

    $scope.uploadData = function(){
        var indexCategory = 0;
        var indexBrand = 0;
        var category = document.getElementById('category').value;
        var brand = document.getElementById('brand').value;

        // var list = [];
        // var ref = firebase.database().ref('products').on('value', function(snapshot){
        //     var list = snapshot.val();
        //     var targetList = [];
        //     for (var index in list){
        //         var a = list[index];
        //         if (category in a){
        //             var b = a[category];
        //             for (var index in b){
        //                 var c = b[index];
        //
        //                 if (brand in c){
        //                     targetList = c[brand];
        //                     var index = Object.keys(targetList).length;
        //                     console.log(index);
        //                     targetList[index] = $scope.newproduct;
        //
        //                     console.log(targetList);
        //                 }
        //             }
        //         }
        //     };
        // });
        //
        // ref.set(list);




        if (category == 'mobile'){
            indexCategory = 0;
            if (brand == 'apple'){
                indexBrand = 0;
            }else if (brand == 'samsung'){
                indexBrand = 1;
            }else if (brand == 'htc'){
                indexBrand = 2;
            }else if (brand == 'sony'){
                indexBrand = 3;
            }else if (brand == 'oppo'){
                indexBrand = 4;
            };
        }else if (category == 'tablet'){
            indexCategory = 1;
            if (brand == 'apple'){
                indexBrand = 0;
            }else if (brand == 'samsung'){
                indexBrand = 1;
            }else if (brand == 'asus'){
                indexBrand = 2;
            };
        }else if (category == 'laptop'){
            indexCategory = 2;
            if (brand == 'dell'){
                indexBrand = 0;
            }else if (brand == 'asus'){
                indexBrand = 1;
            }else if (brand == 'hp'){
                indexBrand = 2;
            }else if (brand == 'lenovo'){
                indexBrand = 3;
            };
        }

        var targetList = firebase.database().ref('products/' + indexCategory + '/' + category + '/' + indexBrand + '/' + brand);
        targetList.on('value', function(snapshot){
            var list = snapshot.val();
            var index = Object.keys(list).length;
            console.log(index);
            targetList.off();
            var newProduct = targetList.child(index);
            newProduct.set($scope.newproduct);
            $scope.$apply(function(){
                $scope.isProcess = true;
            });
        });
    }

    $scope.colors = [''];
    $scope.addColor = function(){
        var color = '';
        $scope.colors.push(color);
    };
    $scope.removeColor = function(){
        if ($scope.colors.length > 1){
            $scope.colors.pop();
        }

    }

    $scope.sizes = [''];
    $scope.addSize = function(){
        var size = '';
        $scope.sizes.push(size);
    };
    $scope.removeSize = function(){
        if ($scope.sizes.length > 1){
            $scope.sizes.pop();
        }
    }



    $scope.reload = function(){
        console.log('click');
        location.reload();
    }
});

app.controller('ModifyProductController', function($scope, $stateParams){
    $scope.products = JSON.parse(localStorage.getItem('products'));
    for (var key in $scope.products){

        var product = $scope.products[key];
        if (product.id == $stateParams.productID){
            $scope.targetProduct = product;
        }
    }

    document.getElementById('id').value = $scope.targetProduct.id;
    document.getElementById('name').value = $scope.targetProduct.name;
    document.getElementById('price').value = $scope.targetProduct.newprice;
    document.getElementById('availability').value = $scope.targetProduct.availability;
    if ($scope.targetProduct.id.substring(0,2) == 'mb'){
        document.getElementById('category').value = 'mobile';
    }else if ($scope.targetProduct.id.substring(0,2) == 'tl') {
        document.getElementById('category').value = 'tablet';
    }else {
        document.getElementById('category').value = 'laptop';
    }

    document.getElementById('brand').value = $scope.targetProduct.brand.toLowerCase();
    document.getElementById('weight').value = $scope.targetProduct.weight.split('g')[0];
    document.getElementById('star').value = $scope.targetProduct.star;
    document.getElementById('sale').value = $scope.targetProduct.sale.split('%')[0];

    var dimensions = $scope.targetProduct.dimension.split(' ');
    console.log(dimensions);
    document.getElementById('dimension_1').value = dimensions[0];
    document.getElementById('dimension_2').value = dimensions[2];
    document.getElementById('dimension_3').value = dimensions[4];

    $scope.colors = $scope.targetProduct.colors;
    $scope.sizes = $scope.targetProduct.sizes;

    for (var index in $scope.targetProduct.overview){
        var overviewline = $scope.targetProduct.overview[index];
        document.getElementById('overview').value += overviewline + '\n';
    }

    for (var index in $scope.targetProduct.description){
        var descriptionline = $scope.targetProduct.description[index];
        document.getElementById('description').value += descriptionline + '\n';
    }


    $scope.colors = $scope.targetProduct.colors;
    $scope.addColor = function(){
        var color = '';
        $scope.colors.push(color);
    };
    $scope.removeColor = function(){
        if ($scope.colors.length > 1){
            $scope.colors.pop();
        }

    }

    $scope.sizes = $scope.targetProduct.sizes;
    $scope.addSize = function(){
        var size = '';
        $scope.sizes.push(size);
    };
    $scope.removeSize = function(){
        if ($scope.sizes.length > 1){
            $scope.sizes.pop();
        }
    }

    $scope.isProcess = true;

    $scope.submitProduct = function(){
        $scope.isProcess = false;

        $scope.targetProduct.name = document.getElementById('name').value;
        $scope.targetProduct.oldprice = $scope.targetProduct.newprice;
        $scope.targetProduct.newprice = parseInt(document.getElementById('price').value, 10);
        $scope.targetProduct.availability = document.getElementById('availability').value;
        $scope.targetProduct.weight = document.getElementById('weight').value + 'g';
        $scope.targetProduct.brand = document.getElementById('brand').value;
        $scope.targetProduct.star = parseInt(document.getElementById('star').value, 10);
        $scope.targetProduct.sale = document.getElementById('sale').value + '%';
        $scope.targetProduct.dimension = document.getElementById('dimension_1').value + ' x ' + document.getElementById('dimension_2').value + ' x ' + document.getElementById('dimension_3').value + ' cm';
        $scope.targetProduct.colors = $scope.colors;
        $scope.targetProduct.sizes = $scope.sizes;
        $scope.targetProduct.overview = document.getElementById('overview').value.split('\n');
        $scope.targetProduct.description = document.getElementById('description').value.split('\n');
        $scope.targetProduct.id = document.getElementById('id').value;

        $scope.uploadAllFile();
    }

    $scope.uploadAllFile = function(){
        var file_large_1 = document.getElementById('large_1').files[0];

        if (file_large_1 != undefined){
            var storageRef = firebase.storage().ref();

            var upload_large_Task_1 = storageRef.child('images/' + file_large_1.name).put(file_large_1);
            upload_large_Task_1.on('state_changed', function(snapshot){

            }, function(error){
                console.log(error);
            }, function(){
                var downloadUrl = upload_large_Task_1.snapshot.downloadURL;
                console.log(downloadUrl);
                $scope.targetProduct.images[0] = downloadUrl;
                $scope.uploadData();
            });
        }else {
            $scope.uploadData();
        }
    };

    $scope.uploadData = function () {
        var indexCategory = 0;
        var indexBrand = 0;
        var category = document.getElementById('category').value;
        var brand = document.getElementById('brand').value;

        if (category == 'mobile'){
            indexCategory = 0;
            if (brand == 'apple'){
                indexBrand = 0;
            }else if (brand == 'samsung'){
                indexBrand = 1;
            }else if (brand == 'htc'){
                indexBrand = 2;
            }else if (brand == 'sony'){
                indexBrand = 3;
            }else if (brand == 'oppo'){
                indexBrand = 4;
            };
        }else if (category == 'tablet'){
            indexCategory = 1;
            if (brand == 'apple'){
                indexBrand = 0;
            }else if (brand == 'samsung'){
                indexBrand = 1;
            }else if (brand == 'asus'){
                indexBrand = 2;
            };
        }else if (category == 'laptop'){
            indexCategory = 2;
            if (brand == 'dell'){
                indexBrand = 0;
            }else if (brand == 'asus'){
                indexBrand = 1;
            }else if (brand == 'hp'){
                indexBrand = 2;
            }else if (brand == 'lenovo'){
                indexBrand = 3;
            };
        }

        var targetList = firebase.database().ref('products/' + indexCategory + '/' + category + '/' + indexBrand + '/' + brand);
        targetList.on('value', function(snapshot){
            var list = snapshot.val();
            for (var index in list) {
                if (list[index].id == $scope.targetProduct.id) {
                    targetList.off();
                    var newProduct = targetList.child(index);
                    newProduct.set($scope.targetProduct);

                    var phase = $scope.$root.$$phase;
                    if (phase == '$apply' || phase == '$digest') {
                        console.log('ok');
                        var processing = document.getElementById('processing');
                        processing.setAttribute('color', 'blue');
                        processing.innerHTML = 'Đổi thông tin sản phẩm thành công';
                    }else {
                        $scope.$apply(function(){
                            var processing = document.getElementById('processing');
                            processing.setAttribute('color', 'blue');
                            processing.innerHTML = 'Đổi thông tin sản phẩm thành công';
                        });
                    }

                    break;
                }
            }
        });
    }

    $scope.reload = function(){
        location.reload();
    }
});

app.controller('LoginController', function($scope, $state, principal){
    $scope.email = '';
    $scope.password = '';
    $scope.HideHeader();


    var errorCode = '';
    var errorMessage = '';

    $scope.isCorrect = true;

    $scope.submitLogin = function(){

        firebase.auth().signInWithEmailAndPassword($scope.email, $scope.password).catch(function(error) {
            // Handle Errors here.
            errorCode = error.code;
            errorMessage = error.message;
            console.log(errorCode + ' ' + errorMessage);
            $scope.$apply(function(){
                $scope.isCorrect = false;
            });
            // ...
        }).then(function(){
            if (errorCode == '') {
                $scope.$apply(function(){
                    $scope.isCorrect = true;
                });

                principal.authenticate({
                    name: $scope.email,
                    roles: ['Admin']
                });

                localStorage.setItem('currentEmail', $scope.email);

                if ($scope.returnToState) {
                    $state.go($scope.returnToState.name, $scope.returnToStateParams);
                }else {
                    $state.go('main');
                }
            }
            errorCode = '';
        });

    }
});

app.controller('ChangePassController', function($scope){
    $scope.currentEmail = localStorage.getItem('currentEmail');
    $scope.inputEmail = '';
    $scope.resetPassword = '';
    $scope.confirmPassword = '';

    $scope.isNotify = false;

    $scope.submitChangePass = function(){
        var notify = document.getElementById('notify');
        notify.setAttribute('color', 'red');
        console.log($scope.currentEmail);
        $scope.isNotify = true;
        if ($scope.inputEmail != $scope.currentEmail){
            notify.innerHTML = 'Email không đúng';
        }else {
            if ($scope.resetPassword != $scope.confirmPassword){
                notify.innerHTML = 'Mật khẩu xác nhận không trùng khớp';
            }
            else {
                var user = firebase.auth().currentUser;
                console.log(user);
                user.updatePassword($scope.resetPassword).then(function(){
                    notify.setAttribute('color', 'blue');
                    notify.innerHTML = 'Đổi mật khẩu thành công';

                    $scope.$apply(function(){
                        $scope.inputEmail = '';
                        $scope.resetPassword = '';
                        $scope.confirmPassword = '';
                    })

                }, function(error){
                    console.log(error);
                    notify.innerHTML = 'Đổi mật khẩu thất bại (Mật khẩu ít nhất 6 ký tự)';
                })
            }
        }
    }
});