/**
 * Created by hungmai on 06/06/2016.
 */
var app = angular.module('Ecommerce', ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'home.html',
            controller: 'HomeController',
            resolve: {
                Products: ['$q', function ($q) {
                    var deferred = $q.defer();
                    var products = [];

                    firebase.database().ref('products').on('value', function (snapshot) {
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[0].apple);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[1].samsung);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[2].htc);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[3].sony);
                        Array.prototype.push.apply(products, snapshot.val()[0].mobile[4].oppo);
                        Array.prototype.push.apply(products, snapshot.val()[1].tablet[0].apple);
                        Array.prototype.push.apply(products, snapshot.val()[1].tablet[1].samsung);
                        Array.prototype.push.apply(products, snapshot.val()[1].tablet[2].asus);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[0].dell);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[1].asus);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[2].hp);
                        Array.prototype.push.apply(products, snapshot.val()[2].laptop[3].lenovo);
                        deferred.resolve(products);
                    });

                    return deferred.promise;
                }]
            }
        })
        .state('products', {
            url: '/products/:cateID/:category/:manuID/:manufacture',
            templateUrl: 'category.html',
            controller: 'CategoryController',
            resolve: {
                Category: ['$q', '$stateParams', function ($q, $stateParams) {
                    var deferred = $q.defer();
                    var products = [];
                    var cateID = $stateParams.cateID;
                    var category = $stateParams.category;
                    var manuID = $stateParams.manuID;
                    var manufacture = $stateParams.manufacture;

                    firebase.database().ref('products/' + cateID + '/' + category + '/' + manuID + '/' + manufacture).on('value', function (snapshot) {
                        var products = snapshot.val();
                        deferred.resolve(products);
                    });

                    return deferred.promise;
                }]
            }
        })
        .state('productdetail', {
            url: '/productdetail/:productID',
            templateUrl: 'product.html',
            controller: 'ProductDetailController'
        })
        .state('contact', {
            url: '/contact',
            templateUrl: 'contact.html',
            controller: 'ContactController'
        })
        .state('compare', {
            url: '/compare',
            templateUrl: 'compare-products.html',
            controller: 'CompareController'
        });
}]);

app.controller('MainController', function($scope) {
    $scope.filterName = '';
    $scope.changeFilterName = function(filterName){
        $scope.filterName = document.getElementById('text-search').value;
    };
    
    $scope.addToCompare = function(product){
        console.log(product);
        $scope.compareProducts = JSON.parse(localStorage.getItem('compareProducts'));
        
        if ($scope.compareProducts == null){
            $scope.compareProducts = [];
            console.log($scope.compareProducts);
        }
        
        if ($scope.compareProducts.length == 3){
            alert('Đã đủ 3 sản phẩm so sánh');
            return;
        }
        for (var key in $scope.compareProducts){
            var compareProduct = $scope.compareProducts[key];
            if (compareProduct.name == product.name) {
                alert('Sản phẩm này đã được thêm vào so sánh');
                return;
            }
        }
        $scope.compareProducts.push(product);
        localStorage.setItem('compareProducts', JSON.stringify($scope.compareProducts));
        alert('Đã thêm sản phẩm vào so sánh');
    };

    $scope.numberFormat = function (number, _sep) {
        var _number = number.toString()
        _number = typeof _number != "undefined" && _number > 0 ? _number : "";
        _number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
        if(typeof _sep != "undefined" && _sep != " ") {
            _number = _number.replace(/\s/g, _sep);
        }
        return _number;
    }
});

app.controller('HomeController', function ($scope, $timeout, Products) {
    $timeout(function () {
        $scope.products = Products;
        $scope.saleProducts = [];
        Array.prototype.push.apply($scope.saleProducts, Products);
        $scope.products.sort(function () {
            if (Math.random() < .5) return -1; else return 1;
        });

        $scope.saleProducts.sort(function () {
            if (Math.random() < .5) return -1; else return 1;
        });

        localStorage.setItem('products', JSON.stringify($scope.products));
        localStorage.setItem('saleProducts', JSON.stringify($scope.saleProducts));

        $scope.filterByName = function(element){
            var n = element.name.toLowerCase().search($scope.filterName.toLowerCase());
            if (n < 0){
                return false;
            }
            return true;
        };

        $scope.checkOldPrice = function(oldprice){
            if (oldprice == 0){
                return true;
            }

            return false;
        }

        $scope.checkSale = function(product){
            console.log(product.sale)
            if (product.sale != ""){
                return true;
            }

            return false;
        }

        var options = {
                nextButton: true,
                prevButton: true,
                pagination: true,
                autoPlay: true,
                autoPlayDelay: 8500,
                pauseOnHover: true,
                preloader: true,
                theme: 'slide',
                speed: 700,
                animateStartingFrameIn: true
            },
            homeSlider = $('#slider-sequence').sequence(options).data("sequence");

        $.getScript("js/main.js")
            .done(function (script, textStatus) {
                console.log(textStatus);
            })
            .fail(function (jqxhr, settings, exception) {
                $("div.log").text("Triggered ajaxError handler.");
            });
    }, 500);
});

app.controller('CategoryController', function ($scope, $timeout, Category, $stateParams) {
    $timeout(function () {
        $.getScript("js/main.js")
            .done(function (script, textStatus) {
                console.log(textStatus);
            })
            .fail(function (jqxhr, settings, exception) {
                $("div.log").text("Triggered ajaxError handler.");
            });
    }, 500);

    $scope.products = Category;

    $scope.saleProducts = JSON.parse(localStorage.getItem('saleProducts'));

    $scope.manufactures = [];
    if ($stateParams.category == 'mobile') {
        $scope.manufactures = [{'manuID': 0, 'manufacture': 'apple'}, {'manuID': 1, 'manufacture': 'samsung'}, {
            'manuID': 2,
            'manufacture': 'htc'
        }, {'manuID': 3, 'manufacture': 'sony'}, {'manuID': 4, 'manufacture': 'oppo'}];
    } else if ($stateParams.category == 'tablet') {
        $scope.manufactures = [{'manuID': 0, 'manufacture': 'apple'}, {'manuID': 1, 'manufacture': 'samsung'}, {
            'manuID': 2,
            'manufacture': 'asus'
        }];
    } else if ($stateParams.category == 'laptop') {
        $scope.manufactures = [{'manuID': 0, 'manufacture': 'dell'}, {'manuID': 1, 'manufacture': 'asus'}, {
            'manuID': 2,
            'manufacture': 'hp'
        }, {'manuID': 3, 'manufacture': 'lenovo'}];
    }
    ;
    $scope.cateID = $stateParams.cateID;
    $scope.category = $stateParams.category;

    console.log($scope.products.length);
    var nPage = parseInt($scope.products.length / 10 + 1);
    $scope.getNumber = function () {
        console.log(nPage);
        var array = new Array(nPage);
        return array;
    };

    $scope.products.sort(function(a, b){
        return parseFloat(a.newprice) - parseFloat(b.newprice);
    });

    $scope.firstSelect = 'Tiền tăng dần';
    $scope.secondSelect = 'Tiền giảm dần';
    $scope.sortByMoney = function(){
        if ($scope.firstSelect == 'Tiền tăng dần'){
            $scope.products.sort(function(a, b){
                return parseFloat(b.newprice) - parseFloat(a.newprice);
            });
            $scope.firstSelect = 'Tiền giảm dần';
            $scope.secondSelect = 'Tiền tăng dần';
        } else {
            $scope.products.sort(function(a, b){
                return parseFloat(a.newprice) - parseFloat(b.newprice);
            });
            $scope.firstSelect = 'Tiền tăng dần';
            $scope.secondSelect = 'Tiền giảm dần';
        }

    };

    $scope.startIndex = 0;
    $scope.getStartIndex = function(pageIndex){
        $scope.startIndex = pageIndex * 9;
    }

    $scope.fromRange = 0;
    $scope.toRange = 1000000000;
    $scope.setRangeMoney = function(){
        $scope.fromRange = document.getElementById('price-range-low').value * 1000000;
        $scope.toRange = document.getElementById('price-range-high').value * 1000000;
    };
    $scope.clearRange = function(){
        $scope.fromRange = 0;
        $scope.toRange = 1000000000;
        document.getElementById('price-range-low').value = '';
        document.getElementById('price-range-high').value = '';
    }
    $scope.filterFromRangeMoney = function(element){
        if (parseInt(element.newprice) >= parseInt($scope.fromRange) && parseInt(element.newprice) <= parseInt($scope.toRange)){

            return true;
        } else {
            return false;
        }
    }
});

app.controller('ProductDetailController', function ($scope, $stateParams, $timeout) {

    $scope.products = JSON.parse(localStorage.getItem('products'));
    for (var key in $scope.products) {
        var product = $scope.products[key];
        if ($stateParams.productID == product.name) {
            $scope.product = product;
            $scope.tabContent = $scope.product.overview;
        }
    }

    $scope.saleProducts = JSON.parse(localStorage.getItem('saleProducts'));

    $timeout(function () {
        $.getScript("js/main.js")
            .done(function (script, textStatus) {
                console.log(textStatus);
            })
            .fail(function (jqxhr, settings, exception) {
                $("div.log").text("Triggered ajaxError handler.");
            });

        $(function() {

            var carouselContainer = $('#product-carousel'),
                productImg =  $('#product-image');

            carouselContainer.elastislide({
                orientation : 'vertical',
                minItems : 4

            });

            productImg.mlens({
                imgSrc: $("#product-image").attr("data-big"),
                lensShape: "square",
                lensSize: 150,
                borderSize: 4,
                borderColor: "#999",
                borderRadius: 0
            });


            var oldImg = productImg.attr('src');
            carouselContainer.find('li').on('mouseover', function() {

                var newImg = $(this).find('a').attr('href');

                productImg.attr({'src': newImg, 'data-big': newImg}).trigger('imagechanged');

            });

            // triggered with custom event
            productImg.on('imagechanged', function() {
                productImg.mlens("update", 0 ,{
                    imgSrc: productImg.attr("data-big"),
                    lensShape: "square",
                    lensSize: 150,
                    borderSize: 4,
                    borderColor: "#999"
                });
            });

        });
        
    }, 500);

    $scope.countProduct = 1;

    $scope.increaseProduct = function () {
        $scope.countProduct++;
    };

    $scope.decreaseProduct = function () {
        if ($scope.countProduct > 1) {
            $scope.countProduct--;
        }

    };

    $scope.getOverview = function () {

        $scope.tabContent = $scope.product.overview;
    };
    $scope.getDescription = function () {
        $scope.tabContent = $scope.product.description;
    };
});

app.controller('CompareController', function($scope, $timeout) {
    $timeout(function () {
        $.getScript("js/main.js")
            .done(function (script, textStatus) {
                console.log(textStatus);
            })
            .fail(function (jqxhr, settings, exception) {
                $("div.log").text("Triggered ajaxError handler.");
            });
    }, 500);

    $scope.products = JSON.parse(localStorage.getItem('compareProducts'));

    $scope.removeCompareProduct = function(index) {
        $scope.products.splice(index, 1);
        localStorage.setItem('compareProducts', JSON.stringify($scope.products));
        console.log($scope.products);
    };
});

app.controller('ContactController', function($scope,$timeout) {
    $timeout(function () {
        $.getScript("js/main.js")
            .done(function (script, textStatus) {
                console.log(textStatus);
            })
            .fail(function (jqxhr, settings, exception) {
                $("div.log").text("Triggered ajaxError handler.");
            });

        $scope.map;
        $scope.markers = [];
        $scope.markerId = 1;

        var latlng = new google.maps.LatLng(10.866401, 106.639458);
        var myOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $scope.map = new google.maps.Map(document.getElementById("map"), myOptions);
        $scope.overlay = new google.maps.OverlayView();
        $scope.overlay.onAdd = function() {};
        $scope.overlay.draw = function() {}; // empty function required
        $scope.overlay.setMap($scope.map);

        var marker = new google.maps.Marker({
            position: latlng,
            map: $scope.map,
            title: 'Hello World!'
        });

    }, 2000);
});